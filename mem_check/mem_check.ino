
/*
   mem_check.ino

   Jacob Kunnappally
   September 2017

   What this program does:

   Setup
   -define memory address constants
   -declare utility globals
   -open serial comms and prime feedback LED for output

   Part 1
   -Scan through SRAM and, starting from end of memory,
    find where 0xFFs stop; this is the end of usable
    memory. Quick check that SRAM_BEGIN is usable so no
    lies when reporting effective memory range
   -Do same for FLASH to find boundaries

   Part 2
   -In the usable bounds of SRAM and FLASH, look for
   0xFFs. When that happens



*/

#define BOARD_LED_PIN 13
#define FLASH_BEGIN (char *)0x08000000
#define FLASH_END (char *)0x0801FFFF
#define SRAM_BEGIN (char *)0x20000000
#define SRAM_END (char *)0x20004FFF

struct bounds
{
  char *start_addr;
  char *stop_addr;
};

bool pinValue = LOW;

void boundary_scan(bounds *mem_bounds);
void corruption_scan(bounds *mem_bounds);
void wait_for_user();

void setup()
{
  Serial.begin(115200);
  pinMode(BOARD_LED_PIN, OUTPUT);

  while (!Serial) {
    delay(10); // wait for serial port to connect. Needed for native USB
  }
  Serial.flush();
  Serial.println("Starting...");
  delay(2000);
}

void loop()
{

  bounds flash_bounds = {FLASH_BEGIN, FLASH_END};
  bounds sram_bounds = {SRAM_BEGIN, SRAM_END};

  Serial.flush();
  delay(10);

  Serial.println("Beginning flash boundary scan...");
  boundary_scan(&flash_bounds);
  delay(10);

  Serial.println("Beginning sram boundary scan...");
  boundary_scan(&sram_bounds);
  delay(10);

  Serial.println("Beginning flash corruption scan...");
  corruption_scan(&flash_bounds);
  delay(10);

  Serial.println("Beginning SRAM corruption scan...");
  corruption_scan(&sram_bounds);
  delay(10);

  Serial.println("Done. Press any key to repeat scans");
  wait_for_user();
  Serial.println("Starting over...");
  delay(10);
}

void boundary_scan(bounds *mem_bounds)
{ //can scan up

  char *mem_loc = mem_bounds->start_addr, *begin_of_end = mem_bounds->stop_addr;
  bool first_found = HIGH;

  while (mem_loc <= mem_bounds->stop_addr)
  {
    if (*mem_loc == 0xFF && first_found)
    { //FF found and start flag hasn't tripped yet
      begin_of_end = mem_loc;
      first_found = LOW;
    }
    else if (*mem_loc != 0xFF)
    {
      first_found = HIGH;
    }

    mem_loc++;
  }

  mem_bounds->stop_addr = --begin_of_end;

  Serial.print("Start addr: 0x");
  Serial.println((int)mem_bounds->start_addr, HEX);
  Serial.print("Stop addr: 0x");
  Serial.println((int)mem_bounds->stop_addr, HEX);
  Serial.print("Memory Range: ");
  Serial.print(mem_bounds->stop_addr - mem_bounds->start_addr);
  Serial.println(" bytes");
  delay(10);
  return;
}

void corruption_scan(bounds *mem_bounds)
{
  char *mem_loc = mem_bounds->start_addr;
  int fault_counter = 0, mem_val;

  while (mem_loc <= mem_bounds->stop_addr)
  { //the following is risky in that I might be overwriting program
    //instructions, but I eventually rewrite to original value if
    //there is no problem
    if (*mem_loc == 0xFF)
    {
      //      *mem_loc = 0x00;
      //
      //      if((mem_val=*mem_loc) != 0x00)
      //      {
      //         Serial.print("Memory corruption at 0x");
      //         Serial.println((int)mem_loc,HEX);
      //         Serial.print("Read back 0x");
      //         Serial.print(mem_val);
      //         Serial.println(" instead of 0x00");
      fault_counter++;
      //      }
      //      else
      //      {
      //        *mem_loc = 0xFF;
      //      }
    }
    mem_loc++;
  }

  Serial.print("0xFFs found: ");
  Serial.println(fault_counter);
  delay(10);
  return;
}

void wait_for_user()
{
  int serial = -1;

  Serial.flush();
  Serial.print("Waiting ");
  for (int i = 0; i < 3; i++)
  {
    delay(333);
    Serial.print(". ");
  }

  while ((serial = Serial.read()) == -1)
  {
    delay(250);
  }

  delay(10);
  return;
}

